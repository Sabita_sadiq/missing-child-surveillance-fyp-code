import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart' as path;
import 'package:sign_in_flutter/api_link.dart';
import 'login_page.dart';
import 'sign_in.dart';
import 'try.dart';
import 'dart:convert';
import 'api_link.dart';

import 'dart:io';

class Notification_user1 extends StatefulWidget {
  final String searchname;
  Notification_user1({Key key, @required this.searchname}) : super(key: key);

  NotificationState1 createState() => NotificationState1(searchname);
}

class NotificationState1 extends State<Notification_user1> {
  @override
  final String searchname;
  NotificationState1(this.searchname);
  final dbRef = FirebaseDatabase.instance.reference().child("Child_Data");
 List<dynamic> list;
  List<Map<dynamic, dynamic>> lists = [];

   bool done1 =false;
       void initState() {
 get_server_image(searchname);
    // This is the initial data // Set it in initState because you are using a stateful widget
super.initState();
  }
  // String url =
 
  get_server_image(String cname) async
  {
    print(cname);
    var uri = Uri.parse( uri2 + '/geturls?name=$cname');
      var result = await http.get(uri);
      var data = json.decode(result.body);
     var res = data['url'] as List;
     print(res);
    setState(() {
      list = res;
      done1 = true;
    
    });
     print(list.length);
     
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          title: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                IconButton(
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
                Spacer(),
                Text(
                  "Child Report",
                  textAlign: TextAlign.center,
                ),
                Spacer(),
                IconButton(
                  icon: Icon(
                    Icons.logout,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    signOutGoogle();
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(builder: (context) {
                      return LoginPage();
                    }), ModalRoute.withName('/'));
                  },
                ),
              ]),
          backgroundColor: Colors.black,
          automaticallyImplyLeading: false,
          elevation: 5.0,
        ),
        body: FutureBuilder(
            future: dbRef.orderByChild("cname").equalTo("$searchname").once(),
            builder: (context, AsyncSnapshot<DataSnapshot> snapshot) {
              if (snapshot.hasData) {
                lists.clear();
               
                Map<dynamic, dynamic> values = snapshot.data.value;
                values.forEach((key, values) {
                
                  //  print(urls);
                  //print(urls.length);
                 // getImage();
                //  get_server_image(searchname);
                  lists.add(values);
                });
               return new ListView.builder(
                    shrinkWrap: true,
                    itemCount: lists.length,

                    //scrollDirection: Axis.horizontal,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        // width: double.infinity,
                        margin: const EdgeInsets.only(
                            top: 50.0, left: 5.0, right: 5.0, bottom: 50.0),
                        padding: EdgeInsets.all(0),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(color: Colors.blue),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          //mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Column(
                              //  mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                SizedBox(height: 20.0),
                              
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.all(10),
                                      child: Text(
                                        "Name: " + lists[index]["cname"],
                                        style: TextStyle(
                                            color: Colors.black, fontSize: 20),
                                      ),
                                    ),
                                  ],
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Age: " + lists[index]["cage"],
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 20),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      "Gender: " + lists[index]["cgender"],
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 20),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    new RichText(
                                        text: TextSpan(
                                      children: [
                                        new TextSpan(
                                            text: "Reporting Email: ",
                                            style: new TextStyle(
                                                color: Colors.black,
                                                //fontWeight: FontWeight.bold,
                                                fontSize: 20)),
                                        new TextSpan(
                                            text: lists[index]
                                                ["Reporting Email"],
                                            style: new TextStyle(
                                                color: Colors.blue,
                                                fontSize: 18),
                                            recognizer: TapGestureRecognizer()
                                              ..onTap = () async {
                                                showDialog(
                                                    context: context,
                                                    builder:
                                                        (BuildContext context) {
                                                      return Dialog(
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        10.0)),
                                                        child: Container(
                                                          padding:
                                                              EdgeInsets.all(
                                                                  20),
                                                          height: 200,
                                                          decoration:
                                                              BoxDecoration(
                                                            border: Border.all(
                                                                color: Colors
                                                                    .blue),
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        10),
                                                          ),
                                                          child: Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .start,
                                                            children: <Widget>[
                                                              Column(
                                                                children: <
                                                                    Widget>[
                                                                  SizedBox(
                                                                      height:
                                                                          16.0),
                                                                  Text(
                                                                    "Reporting Person Information",
                                                                    style:
                                                                        TextStyle(
                                                                      fontSize:
                                                                          15,
                                                                    ),
                                                                  ),
                                                                  SizedBox(
                                                                      height:
                                                                          16.0),
                                                                  Row(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .center,
                                                                    children: [
                                                                      Icon(
                                                                        Icons
                                                                            .account_box,
                                                                        color: Colors
                                                                            .blue,
                                                                        size:
                                                                            24.0,
                                                                      ),
                                                                      Text(
                                                                        "   Name : " +
                                                                            lists[index]["Reporting Person Name"],
                                                                        style: TextStyle(
                                                                            color:
                                                                                Colors.black,
                                                                            fontWeight: FontWeight.bold,
                                                                            fontSize: 14),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                  Row(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .center,
                                                                    children: [
                                                                      Icon(
                                                                        Icons
                                                                            .add_ic_call,
                                                                        color: Colors
                                                                            .blue,
                                                                        size:
                                                                            24.0,
                                                                      ),
                                                                      Text(
                                                                        "   Phone: " +
                                                                            lists[index]["Reporting Person Phone"],
                                                                        style: TextStyle(
                                                                            color:
                                                                                Colors.black,
                                                                            fontWeight: FontWeight.bold,
                                                                            fontSize: 14),
                                                                      ),
                                                                    ],
                                                                  )
                                                                ],
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      );
                                                    });
                                                // Navigator.push(
                                                //     context,
                                                //     MaterialPageRoute(
                                                //         builder: (context) =>
                                                //             Parent_data(
                                                //                 parent_email:
                                                //                     lists[index]
                                                //                         [
                                                //                         "Reporting Email"])));
                                              })
                                      ],
                                    )),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Column(
                                      children: [
                                        Text(
                                          "Last Seen Date: " +
                                              lists[index]["last_date"],
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 20),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Column(
                                      children: [
                                        Text(
                                          "Last Seen Time: " +
                                              lists[index]["last_time"],
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 20),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Column(
                                      children: [
                                        Text(
                                          "Place Missing from: " +
                                              lists[index]["last_place"],
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 20),
                                          textAlign: TextAlign.center,
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 50,
                                ),
                   
                              ],
                            ),
                          ],
                        ),
                      );
                    }); 
              }
              return Center(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircularProgressIndicator(),
                ],
              ));
            }));
  }
}
