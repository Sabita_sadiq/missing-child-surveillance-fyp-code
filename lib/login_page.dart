import 'package:flutter/material.dart';
import 'FirstScreen2.dart';
import 'package:flutter/cupertino.dart';
import 'admin.dart';
import 'sign_in.dart';
import 'process.dart';
import 'try.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/main.jpg'), fit: BoxFit.cover),
        ),
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 50),
              _signInButton(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _signInButton() {
    return FlatButton(
      splashColor: Colors.white,
      onPressed: () {
        signInWithGoogle().then((result) {
          if (result != null) {
            if(email == "k173659@nu.edu.pk")
            {
               Navigator.of(context).push(
              MaterialPageRoute(
                builder: (
                  context,
                ) {
                  // _showLoadingDialog();
                  return MainPage();
                },
              ),
            );
            }
            else
            {
                Navigator.of(context).push(
              MaterialPageRoute(
                builder: (
                  context,
                ) {
                  // _showLoadingDialog();
                  return MyHomePage(
                    notification_key: 1,
                  );
                },
              ),
            );
            }
           
          }
        });
      },
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
      color: Colors.black,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image(image: AssetImage("assets/google_logo.png"), height: 35.0),
            Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Text(
                'Sign in with Google',
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.white,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildLoading() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          CupertinoActivityIndicator(animating: true),
          SizedBox(height: 10.0),
          Text("Initilizing Form Data"),
        ],
      ),
    );
  }
}
