import 'package:flutter/material.dart';

import 'package:flutter/cupertino.dart';
import 'login_page.dart';
import 'sign_in.dart';
import 'childinfo.dart';

import 'package:firebase_database/firebase_database.dart';

import 'package:validators/validators.dart';


class FirstScreen2 extends StatefulWidget {
  FirstScreen2State createState() => FirstScreen2State();
}

class FirstScreen2State extends State<FirstScreen2> {
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                IconButton(
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
                Spacer(),
                Column(
                  children: [
                      Text(
                  "Reporting Person",
                  textAlign: TextAlign.center,
                ),
                  Text(
                  "Information",
                  textAlign: TextAlign.center,
                ),
                  ],
                ),
                
                Spacer(),
                IconButton(
                  icon: Icon(
                    Icons.logout,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    signOutGoogle();
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(builder: (context) {
                      return LoginPage();
                    }), ModalRoute.withName('/'));
                  },
                ),
              ]),
          backgroundColor: Colors.black,
          automaticallyImplyLeading: false,
          elevation: 5.0,
        ),
        body: Center(child: HomeScreen()));
  }
}

class HomeScreen extends StatefulWidget {
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  final _username = TextEditingController();
  Future<void> _initForm;
  DatabaseReference _ref;
  //CollectionReference users;
  final _email = TextEditingController();
  final _cnic = TextEditingController();
  final _phone = TextEditingController();
  final _address = TextEditingController();
  final _state = TextEditingController();
  final _city = TextEditingController();
  final _relativeNumber = TextEditingController();
  final _policestationname = TextEditingController();

  DateTime _date = null;
  String formatted_date = null;
  final _firnumber = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool priceupdate_value = false;
  bool valuefirst = false;

  bool _autoValidate = false;
  void initState() {
    super.initState();
    _ref = FirebaseDatabase.instance.reference().child("Parent_Data");
  }

  getItemAndNavigate(BuildContext context) {
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => Child_Information(
            parentphone: _phone.text,
          ),
        ));
  }

  _showImageDialog() {
    showDialog(
        context: context,
        builder: (_) => new CupertinoAlertDialog(
              content: new Text("Parent Data Successfully Uploaded "),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            ));
  }

  uploaddata(BuildContext context) {
    Map<String, String> parent_data = {
      'reporting_name': _username.text,
      'reporting_email': email,
      'reporting_cnic': _cnic.text,
      'reporting_phone': _phone.text,
      'reporting_address': _address.text,
      'reporting_state': _state.text,
      'reporting_city': _city.text,
      'Relation_With_Reporting_Person': _relativeNumber.text,
      'fir_number': _firnumber.text,
      'Police_Station': _policestationname.text,
      'fir_date': formatted_date,
    };
    _ref.push().set(parent_data);
    getItemAndNavigate(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: HomePageBody());
  }

  Widget HomePageBody() {
    return Container(
      padding: EdgeInsets.all(16),
      child: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Column(
            //mainAxisAlignment: min,
            children: <Widget>[
              SizedBox(
                height: 16,
              ),
              NameInput(),
              SizedBox(
                height: 16,
              ),
              CNICInput(),
              SizedBox(
                height: 16,
              ),
              EmailInput(),
              SizedBox(
                height: 16,
              ),
              PhoneInput(),
              SizedBox(
                height: 16,
              ),
              AddressInput(),
              SizedBox(
                height: 16,
              ),
              CityInput(),
              SizedBox(
                height: 16,
              ),
              StateInput(),
              SizedBox(
                height: 16,
              ),
              RelativeInput(),
              SizedBox(
                height: 16,
              ),
            
      ElevatedButton.icon(
              onPressed: _validateInputs,
              label: Text('Submit'),
              icon: Icon(Icons.file_upload),
              style: ElevatedButton.styleFrom(
                primary: Colors.black,
                // padding: EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                textStyle: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold)),
            )
      

            ],
          ),
        ),
      ),
    );
  }

  void _validateInputs() {
    if (_formKey.currentState.validate()) {
      
        uploaddata(context);

        _formKey.currentState.save();
      }
    else {
//    If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
      });
    }
  }

  Widget NameInput() {
    return TextFormField(
      controller: _username,
      decoration: const InputDecoration(
          border: OutlineInputBorder(),
          labelText: "Reporting Person's Name",
          labelStyle: TextStyle(color: Colors.black)),
      keyboardType: TextInputType.text,
      validator: (String arg) {
        if (arg.length < 3)
          return 'Name must be more than 2 charater';
        else if (arg == "") {
          return 'Please fill this field';
        } 
        else if(!isAlpha(arg))
        {
          return 'Invalid Name';
        }
        else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }

  Widget EmailInput() {
    return TextFormField(
      enabled: false,
      controller: _email,
      //focusNode: _emailFocusNode,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: email,
          labelStyle: TextStyle(color: Colors.black),
          hintText: email
          //hintText: "e.g abc@gmail.com",
          ),

      textInputAction: TextInputAction.next,

      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }

  Widget CNICInput() {
    return TextFormField(
      controller: _cnic,
      // focusNode: _passwordFocusNode,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: "CNIC",
        hintText: "42201xxxxxxxx No need to add Dash",
        labelStyle: TextStyle(color: Colors.black),
      ),
      textInputAction: TextInputAction.done,
      validator: (String args) {
        if (args == "") {
          return 'Please fill this field';
        } else if (args.length < 13 || args.length > 13)
          return 'Invalid CNIC';
        else if (!isNumeric(args))
        {
         return 'Invalid CNIC'; 
        }
        else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }

  Widget PhoneInput() {
    return TextFormField(
      controller: _phone,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: "Phone",
        labelStyle: TextStyle(color: Colors.black),
        hintText: "03xxxxxxxxx",
      ),
      textInputAction: TextInputAction.done,
      validator: (String args) {
        if (args == "") {
          return 'Please fill this field';
        } else if (args.length < 11 || args.length > 11) {
          return 'Invalid Phone';
        }
        else if (!isNumeric(args))
        {
         return 'Invalid Phone'; 
        } 
        else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }

  Widget AddressInput() {
    return TextFormField(
      controller: _address,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: "Address",
        labelStyle: TextStyle(color: Colors.black),
        hintText: "",
      ),
      textInputAction: TextInputAction.done,
      validator: (String args) {
        if (args == "")
          return 'Please fill this field';
        else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }

  Widget StateInput() {
    return TextFormField(
      controller: _state,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: "State",
        labelStyle: TextStyle(color: Colors.black),
        hintText: "",
      ),
      textInputAction: TextInputAction.done,
      validator: (String args) {
        if (args == "")
          return 'Please fill this field';
        else if (!isAlpha(args)) {
          return 'Invalid State';
        } else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }

  Widget CityInput() {
    return TextFormField(
      controller: _city,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: "City",
        labelStyle: TextStyle(color: Colors.black),
        hintText: "",
      ),
      textInputAction: TextInputAction.done,
      validator: (String args) {
        if (args == "")
          return 'Please fill this field';
        else if (!isAlpha(args)) {
          return 'Invalid City';
        } else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }

  Widget RelativeInput() {
    return TextFormField(
      controller: _relativeNumber,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: "Relation with the missing Child",
        labelStyle: TextStyle(color: Colors.black),
        hintText: "Friend,Neighbor",
      ),
      textInputAction: TextInputAction.done,
      validator: (String args) {
        if (args == "")
          return 'Please fill this field';
        else if (!isAlpha(args))
          return 'Invalid Field';
        else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }




}
