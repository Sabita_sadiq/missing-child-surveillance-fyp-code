import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'notification.dart';
import 'login_page.dart';
import 'sign_in.dart';
import 'dart:convert';
import 'dart:io';
import 'api_link.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart' as path;

class Getall extends StatefulWidget {
  Getall({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _GetallState createState() => _GetallState();
}

class _GetallState extends State<Getall> {
  List<dynamic> list;
  bool done1 = false;
  void initState() {
    get_image();
    // This is the initial data // Set it in initState because you are using a stateful widget
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                IconButton(
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
                Spacer(),
                Text(
                  "All Missing Report",
                  textAlign: TextAlign.center,
                ),
                Spacer(),
                IconButton(
                  icon: Icon(
                    Icons.logout,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    signOutGoogle();
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(builder: (context) {
                      return LoginPage();
                    }), ModalRoute.withName('/'));
                  },
                ),
              ]),
          backgroundColor: Colors.black,
          automaticallyImplyLeading: false,
          elevation: 5.0,
        ),
        body: Container(
          child: done1
              ? new ListView.builder(
                  shrinkWrap: true,
                  itemCount: list.length,
                  //scrollDirection: Axis.horizontal,
                  itemBuilder: (BuildContext context, int index) {
                    return new GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Notification_user(
                                      searchname: list[index][0],
                                    )));
                      },
                      child: Container(
                        margin: const EdgeInsets.only(
                            top: 10.0, left: 5.0, right: 5.0, bottom: 10.0),
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          border: Border(
                              bottom:
                                  BorderSide(width: 1.0, color: Colors.grey)),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          // mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.only(left: 5),
                                  child: CircleAvatar(
                                      radius: 40,
                                      backgroundColor: Colors.transparent,
                                      child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(100),
                                          child: Image.memory(
                                            base64Decode(list[index][1]),
                                            width: 100,
                                            height: 100,
                                            fit: BoxFit.cover,
                                          ))),
                                ),
                                Column(children: [
                                  Padding(
                                      padding: EdgeInsets.only(left: 0),
                                      child: Text(
                                        list[index][0],
                                        style: TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold),
                                      )),
                                  Padding(
                                      padding: EdgeInsets.only(left: 20),
                                      child: Text(
                                        list[index][2],
                                        style: TextStyle(
                                            fontSize: 14, color: Colors.grey),
                                      )),
                                ])
                              ],
                            ),
                          ],
                        ),
                      ),
                    );
                  })
              : CircularProgressIndicator(),
        ));
  }

  get_image() async {
    var uri = Uri.parse(uri2 + '/getall');
    var result = await http.get(uri);
    var data = json.decode(result.body);
    var res = data['url'] as List;
   
    setState(() {
      list = res;
      done1 = true;
    });
  }
}
