// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'login_page.dart';
import 'sign_in.dart';
import 'process.dart';

class Nonoti extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Welcome to Flutter',
      home: Scaffold(
        appBar: AppBar(
          title: null,
          backgroundColor: Colors.black,
          automaticallyImplyLeading: false,
          elevation: 5.0,
        ),
        body: Center(
            child: Column(
          children: [
            SizedBox(
              height: 250,
            ),
            Padding(
              padding: EdgeInsets.all(0),
              child: Text(
                "We are still Searching for the result\n",
                style: TextStyle(color: Colors.black),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(
              height: 0,
            ),
            Padding(
              padding: EdgeInsets.all(0),
              child: Text(
                "Please be patient we will Contact you soon\n",
                style: TextStyle(color: Colors.black, fontSize: 25),
                textAlign: TextAlign.center,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.all(10),
                  child: RaisedButton.icon(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => MyHomePage(
                                notification_key: 0,
                                   
                                  )));
                    },
                    label: Text(
                      "Home Page",
                      style: TextStyle(color: Colors.white),
                    ),
                    icon: Icon(
                      Icons.home,
                      color: Colors.white,
                    ),
                    color: Colors.black,
                  ),
                ),
                RaisedButton.icon(
                  onPressed: () {
                    signOutGoogle();
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(builder: (context) {
                      return LoginPage();
                    }), ModalRoute.withName('/'));
                  },
                  label: Text(
                    "Logout",
                    style: TextStyle(color: Colors.white),
                  ),
                  icon: Icon(
                    Icons.logout,
                    color: Colors.white,
                  ),
                  color: Colors.black,
                ),
              ],
            ),
          ],
        )),
      ),
    );
  }
}
