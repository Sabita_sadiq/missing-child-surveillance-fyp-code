import os
import cv2
from mtcnn import MTCNN

def sdr(x1):
  for filename in os.listdir(x1):
    # Read the input image
    if(filename.endswith(".jpg")):
      print(filename)
      img = cv2.imread(x1+"/"+filename)
      detector = MTCNN()
      detections = detector.detect_faces(img)

      for detection in detections:
        score = detection["confidence"]
        if score >= 0.90:
          x, y, w, h = detection["box"]
          detected_face = img[int(y):int(y+h), int(x):int(x+w)]
          #cv2_imshow(faces)
          filek=x1+'/'+filename
          ll=filek.split('.')[0]
          #fd=ll.split('.')[0]
          os.remove(x1+'/'+filename)
          cv2.imwrite(os.path.join(ll+'.jpg'), detected_face)
      
      # Display the output
   
